//
//  ExhibitViewController.m
//  BWADesign
//
//  Created by Artur Jaworski on 22.05.2014.
//  Copyright (c) 2014 brckt. All rights reserved.
//

#import "ExhibitViewController.h"
#import "BeaconsHelper.h"
#import <AudioToolbox/AudioToolbox.h>

@interface ExhibitViewController ()

@end

@implementation ExhibitViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.titleLabel setFont:[UIFont fontWithName:TextFontName size:Header1FontSize]];
    [self.subtitleLabel setFont:[UIFont fontWithName:TextFontName size:Header2FontSize]];
    [self.textLabel setFont:[UIFont fontWithName:TextFontName size:TextFontSize]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)didChangeLayoutAttributes {
    self.imageView.image = [[BeaconsHelper sharedInstance] imageForBeacon:self.beacon];
    self.titleLabel.text = [[BeaconsHelper sharedInstance] titleForBeacon:self.beacon];
    self.subtitleLabel.text = [[BeaconsHelper sharedInstance] subtitleForBeacon:self.beacon];
    self.textLabel.text = [[BeaconsHelper sharedInstance] descriptionForBeacon:self.beacon];
    [self.textLabel sizeToFit];
    
    // fit scrollview size
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, self.textLabel.frame.origin.y+self.textLabel.frame.size.height+self.titleLabel.frame.origin.y);
}

- (void)setBeacon:(bBeacon *)beacon {
    _beacon = beacon;
    [self didChangeLayoutAttributes];
    [self playSound];
}

- (void)playSound {
    NSURL *fileURL = [NSURL URLWithString:@"/System/Library/Audio/UISounds/Modern/sms_alert_note.caf"];
    SystemSoundID soundID;
    AudioServicesCreateSystemSoundID((__bridge_retained CFURLRef)fileURL,&soundID);
    AudioServicesPlaySystemSound(soundID);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
