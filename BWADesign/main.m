//
//  main.m
//  BWADesign
//
//  Created by Artur Jaworski on 22.05.2014.
//  Copyright (c) 2014 brckt. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
