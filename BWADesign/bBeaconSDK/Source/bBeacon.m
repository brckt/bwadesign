//
//  bBeacon.m
//  bBeaconSDK
//
//  Created by Artur Jaworski on 04.11.2013.
//  Copyright (c) 2013 bBeacon. All rights reserved.
//

#import "bBeacon.h"

@implementation bBeacon

- (id)init {
    self = [super init];
    if(self) {
        _isConnected = NO;
    }
    return self;
}

- (void)connectToBeacon {
    [self.delegate beacon:self connectionDidFail:[NSError errorWithDomain:@"bBeacon" code:400 userInfo:nil]];
}

- (void)disconnectBeacon {
    [self.delegate beacon:self didDisconnectWithError:[NSError errorWithDomain:@"bBeacon" code:401 userInfo:nil]];
}

@end
