//
//  bBeaconDevice.m
//  bBeaconSDK
//
//  Created by Artur Jaworski on 04.11.2013.
//  Copyright (c) 2013 bBeacon. All rights reserved.
//

#import "bBeaconDevice.h"

@interface bBeaconDevice ()

@property (nonatomic, strong) CBPeripheralManager* peripheralManager;

@end

@implementation bBeaconDevice

+ (NSNumber*)defaultPower {
    return @-59;
}

- (id)init {
    self = [super init];
    if(self) {
        self.peripheralManager = [[CBPeripheralManager alloc] initWithDelegate:self queue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)];
    }
    return self;
}

- (void)startAdvertisingWithMajor:(bBeaconMajorValue)major withMinor:(bBeaconMinorValue)minor withIdentifier:(NSString *)identifier {
    bBeaconRegion *region = [[bBeaconRegion alloc] initRegionWithMajor:major minor:minor identifier:identifier];
    NSDictionary *peripheralData = [region peripheralDataWithMeasuredPower:[[self class] defaultPower]];
    [self.peripheralManager startAdvertising:peripheralData];
}

- (void)stopAdvertising {
    [self.peripheralManager stopAdvertising];
}

/* CBPeripheralManagerDelegate */

- (void)peripheralManagerDidStartAdvertising:(CBPeripheralManager *)peripheral error:(NSError *)error {
    if(peripheral != self.peripheralManager) {
        return;
    }
    
    [self.delegate beaconDeviceDidStartAdvertising:self error:error];
}

- (void)peripheralManagerDidUpdateState:(CBPeripheralManager *)peripheral {
    return;
}

@end
