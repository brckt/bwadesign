//
//  bBeaconRegion.m
//  bBeaconSDK
//
//  Created by Artur Jaworski on 04.11.2013.
//  Copyright (c) 2013 bBeacon. All rights reserved.
//

#import "bBeaconRegion.h"
#import "bBeaconManager.h"

@interface bBeaconRegion ()

@end

@implementation bBeaconRegion

- (id)initRegionWithIdentifier:(NSString *)identifier {
    return [super initWithProximityUUID:[[bBeaconManager sharedInstance] bBeaconUUID] identifier:identifier];
}

- (id)initRegionWithMajor:(bBeaconMajorValue)major identifier:(NSString *)identifier {
    return [super initWithProximityUUID:[[bBeaconManager sharedInstance] bBeaconUUID] major:major identifier:identifier];
}

- (id)initRegionWithMajor:(bBeaconMajorValue)major minor:(bBeaconMinorValue)minor identifier:(NSString *)identifier {
    return [super initWithProximityUUID:[[bBeaconManager sharedInstance] bBeaconUUID] major:major minor:minor identifier:identifier];
}

- (id)initWithBeaconRegion:(CLBeaconRegion *)beaconRegion {
    if(beaconRegion.major && beaconRegion.minor) {
        return [self initRegionWithMajor:(bBeaconMajorValue)beaconRegion.major minor:(bBeaconMinorValue)beaconRegion.minor identifier:beaconRegion.identifier];
    }
    else if(beaconRegion.major && !beaconRegion.minor) {
        return [self initRegionWithMajor:(bBeaconMajorValue)beaconRegion.major identifier:beaconRegion.identifier];
    }
    else {
        return [self initRegionWithIdentifier:beaconRegion.identifier];
    }
    
    return nil;
}

@end
