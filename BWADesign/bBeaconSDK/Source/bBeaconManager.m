//
//  bBeaconManager.m
//  bBeaconSDK
//
//  Created by Artur Jaworski on 04.11.2013.
//  Copyright (c) 2013 bBeacon. All rights reserved.
//

#import "bBeaconManager.h"
#import "bBeacon.h"

@interface bBeaconManager ()

@property (nonatomic, strong) CLLocationManager* locationManager;

@end

@implementation bBeaconManager

- (id)init {
    self = [super init];
    if(self) {
        self.showUnknownStateBeacons = YES;
        
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
    }
    return self;
}

+ (instancetype)sharedInstance
{
    static bBeaconManager* sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (NSUUID*)bBeaconUUID {
    return [[NSUUID alloc] initWithUUIDString:@"b9407f30-f5f8-466e-aff9-25556b57fe6d"];
}

- (void)startMonitoringForRegion:(bBeaconRegion *)region {
    [self.locationManager startMonitoringForRegion:region];
}

- (void)stopMonitoringForRegion:(bBeaconRegion *)region {
    [self.locationManager stopMonitoringForRegion:region];
}

- (void)startRangingBeaconsInRegion:(bBeaconRegion *)region {
    [self.locationManager startRangingBeaconsInRegion:region];
}

- (void)stopRangingBeaconsInRegion:(bBeaconRegion *)region {
    [self.locationManager stopRangingBeaconsInRegion:region];
}

- (void)requestStateForRegion:(bBeaconRegion *)region {
    [self.locationManager requestStateForRegion:region];
}

/* CLLocationManagerDelegate */

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region {
    if(![region isKindOfClass:[CLBeaconRegion class]] || manager != self.locationManager) {
        return;
    }
    
    bBeaconRegion *bRegion = [[bBeaconRegion alloc] initWithBeaconRegion:(CLBeaconRegion*)region];
    
    if([self.delegate respondsToSelector:@selector(beaconManager:didEnterRegion:)]) {
        [self.delegate beaconManager:self didEnterRegion:bRegion];
    }
}

- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region {
    if(![region isKindOfClass:[CLBeaconRegion class]] || manager != self.locationManager) {
        return;
    }
    
    bBeaconRegion *bRegion = [[bBeaconRegion alloc] initWithBeaconRegion:(CLBeaconRegion*)region];
    
    if([self.delegate respondsToSelector:@selector(beaconManager:didExitRegion:)]) {
        [self.delegate beaconManager:self didExitRegion:bRegion];
    }
}

- (void)locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region {
    if(![region isKindOfClass:[CLBeaconRegion class]] || manager != self.locationManager) {
        return;
    }
    
    bBeaconRegion *bRegion = [[bBeaconRegion alloc] initWithBeaconRegion:(CLBeaconRegion*)region];
    
    if([self.delegate respondsToSelector:@selector(beaconManager:didDetermineState:forRegion:)]) {
        [self.delegate beaconManager:self didDetermineState:state forRegion:bRegion];
    }
}

- (void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region {
    if(manager != self.locationManager) {
        return;
    }
    
    bBeaconRegion *bRegion = [[bBeaconRegion alloc] initWithBeaconRegion:(CLBeaconRegion*)region];
    
    NSMutableArray *bBeaconsArray = [NSMutableArray array];
    for(CLBeacon* beacon in beacons) {
        bBeacon *tmp = [[bBeacon alloc] init];
        tmp.iBeacon = beacon;
        if(!self.showUnknownStateBeacons && beacon.proximity == CLProximityUnknown) {
            return;
        }
        
        [bBeaconsArray addObject:tmp];
    }
    
    NSArray *bBeacons = [bBeaconsArray copy];
    
    if([self.delegate respondsToSelector:@selector(beaconManager:didRangeBeacons:inRegion:)]) {
        [self.delegate beaconManager:self didRangeBeacons:bBeacons inRegion:bRegion];
    }
}

- (void)locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(CLRegion *)region withError:(NSError *)error {
    if(![region isKindOfClass:[CLBeaconRegion class]] || manager != self.locationManager) {
        return;
    }
    
    bBeaconRegion *bRegion = [[bBeaconRegion alloc] initWithBeaconRegion:(CLBeaconRegion*)region];
    
    if([self.delegate respondsToSelector:@selector(beaconManager:monitoringDidFailForRegion:withError:)]) {
        [self.delegate beaconManager:self monitoringDidFailForRegion:bRegion withError:error];
    }
}

- (void)locationManager:(CLLocationManager *)manager rangingBeaconsDidFailForRegion:(CLBeaconRegion *)region withError:(NSError *)error {
    if(manager != self.locationManager) {
        return;
    }
    
    bBeaconRegion *bRegion = [[bBeaconRegion alloc] initWithBeaconRegion:(CLBeaconRegion*)region];
    
    if([self.delegate respondsToSelector:@selector(beaconManager:rangingBeaconsDidFailForRegion:withError:)]) {
        [self.delegate beaconManager:self rangingBeaconsDidFailForRegion:bRegion withError:error];
    }
}

@end
