//
//  bBeaconDevice.h
//  bBeaconSDK
//
//  Created by Artur Jaworski on 04.11.2013.
//  Copyright (c) 2013 bBeacon. All rights reserved.
//
//  This class represents bBeacon simulator on device.

#import <CoreBluetooth/CoreBluetooth.h>
#import <Foundation/Foundation.h>
#import "bBeaconRegion.h"

@class bBeaconDevice;

@protocol bBeaconDeviceDelegate <NSObject>

@optional

- (void)beaconDeviceDidStartAdvertising:(bBeaconDevice *)device
                                  error:(NSError *)error;

@end

@interface bBeaconDevice : NSObject <CBPeripheralManagerDelegate>

@property (nonatomic, weak) id <bBeaconDeviceDelegate> delegate;

- (void)startAdvertisingWithMajor:(bBeaconMajorValue)major
                        withMinor:(bBeaconMinorValue)minor
                   withIdentifier:(NSString*)identifier;
- (void)stopAdvertising;

@end
