//
//  bBeaconManager.h
//  bBeaconSDK
//
//  Created by Artur Jaworski on 04.11.2013.
//  Copyright (c) 2013 bBeacon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "bBeaconRegion.h"

@class bBeaconManager;

@protocol bBeaconManagerDelegate <NSObject>

@optional

- (void)beaconManager:(bBeaconManager *)manager
      didRangeBeacons:(NSArray *)beacons
             inRegion:(bBeaconRegion *)region;

- (void)beaconManager:(bBeaconManager *)manager
rangingBeaconsDidFailForRegion:(bBeaconRegion *)region
           withError:(NSError *)error;

- (void)beaconManager:(bBeaconManager *)manager
monitoringDidFailForRegion:(bBeaconRegion *)region
           withError:(NSError *)error;

- (void)beaconManager:(bBeaconManager *)manager
      didEnterRegion:(bBeaconRegion *)region;

- (void)beaconManager:(bBeaconManager *)manager
       didExitRegion:(bBeaconRegion *)region;

- (void)beaconManager:(bBeaconManager *)manager
   didDetermineState:(CLRegionState)state
           forRegion:(bBeaconRegion *)region;

@end

@interface bBeaconManager : NSObject <CLLocationManagerDelegate>

@property (nonatomic, weak) id <bBeaconManagerDelegate> delegate;
@property (nonatomic) BOOL showUnknownStateBeacons;

- (void)startRangingBeaconsInRegion:(bBeaconRegion*)region;
- (void)startMonitoringForRegion:(bBeaconRegion*)region;
- (void)stopRangingBeaconsInRegion:(bBeaconRegion*)region;
- (void)stopMonitoringForRegion:(bBeaconRegion *)region;
- (void)requestStateForRegion:(bBeaconRegion *)region;

+ (instancetype)sharedInstance;
- (NSUUID*)bBeaconUUID;

@end
