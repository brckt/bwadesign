//
//  bBeaconRegion.h
//  bBeaconSDK
//
//  Created by Artur Jaworski on 04.11.2013.
//  Copyright (c) 2013 bBeacon. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>

typedef uint16_t bBeaconMajorValue;
typedef uint16_t bBeaconMinorValue;

@interface bBeaconRegion : CLBeaconRegion

- (id)initWithBeaconRegion:(CLBeaconRegion*)beaconRegion;
- (id)initRegionWithIdentifier:(NSString *)identifier;
- (id)initRegionWithMajor:(bBeaconMajorValue)major identifier:(NSString *)identifier;
- (id)initRegionWithMajor:(bBeaconMajorValue)major minor:(bBeaconMinorValue)minor identifier:(NSString *)identifier;

@end