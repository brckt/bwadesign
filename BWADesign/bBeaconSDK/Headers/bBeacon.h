//
//  bBeacon.h
//  bBeaconSDK
//
//  Created by Artur Jaworski on 04.11.2013.
//  Copyright (c) 2013 bBeacon. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import <Foundation/Foundation.h>

@class bBeacon;

@protocol bBeaconDelegate <NSObject>

@optional

- (void)beacon:(bBeacon*)beacon connectionDidFail:(NSError*)error;
- (void)beacon:(bBeacon*)beacon didDisconnectWithError:(NSError*)error;
- (void)beaconConnectionDidSucceeded:(bBeacon*)beacon;

@end

@interface bBeacon : NSObject

@property (nonatomic, weak) id <bBeaconDelegate> delegate;
@property (nonatomic, strong) CLBeacon* iBeacon;

@property (nonatomic, readonly) BOOL isConnected;

- (void)connectToBeacon;
- (void)disconnectBeacon;

@end