//
//  ExplorationViewController.h
//  BWADesign
//
//  Created by Artur Jaworski on 22.05.2014.
//  Copyright (c) 2014 brckt. All rights reserved.
//

#import "AppViewController.h"
#import "bBeaconManager.h"

@interface ExplorationViewController : AppViewController <bBeaconManagerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *instruction;
@property (weak, nonatomic) IBOutlet UIView *exhibitViewControllerWrapper;

@end
