//
//  ExhibitViewController.h
//  BWADesign
//
//  Created by Artur Jaworski on 22.05.2014.
//  Copyright (c) 2014 brckt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "bBeacon.h"

@interface ExhibitViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (nonatomic, strong) bBeacon *beacon;

@end
