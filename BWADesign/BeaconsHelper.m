//
//  BeaconsHelper.m
//  BWA
//
//  Created by Artur Jaworski on 25.05.2014.
//  Copyright (c) 2014 brckt. All rights reserved.
//

#import "BeaconsHelper.h"

@interface BeaconsHelper ()

@property (nonatomic, strong) NSArray *data;
@property (nonatomic, strong) NSMutableArray *usedBeacons;

@end

@implementation BeaconsHelper

+ (instancetype)sharedInstance {
    static BeaconsHelper* sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

+ (BOOL)bBeaconEquals:(bBeacon*)beacon with:(bBeacon*)otherBeacon {
    if([beacon.iBeacon.proximityUUID.UUIDString isEqualToString:otherBeacon.iBeacon.proximityUUID.UUIDString] && [beacon.iBeacon.minor isEqualToNumber:otherBeacon.iBeacon.minor] && [beacon.iBeacon.major isEqualToNumber:otherBeacon.iBeacon.major]) {
        return YES;
    }
    return NO;
}

- (id)init {
    self = [super init];
    if(self) {
        self.usedBeacons = [NSMutableArray array];
#warning it should be taken from db or api
        self.data = @[
                      @{@"title": @"Id singulis forensibus", @"subtitle": @"Raul Guitierrez, 1986", @"description": @"Lorem ipsum dolor sit amet, mundi deserunt moderatius no per, ne adipiscing inciderint sea. Pri ad omnes consul, vis eius epicuri necessitatibus id. Cu tantas consequat eos, ex debitis corpora comprehensam vis, ne sed nostro omnesque neglegentur. Tollit volumus vulputate nec ex, cu pro unum explicari. Iudico option dolorum eum ei, ex duo porro ludus nominavi. Sea in detraxit praesent consequat, cu quaeque feugiat nonumes usu, ludus mandamus referrentur in mel. No vel modo mollis partiendo, facer conclusionemque vituperatoribus duo an."},
                      @{@"title": @"Has at dicta eripuit", @"subtitle": @"Jerico Cabana, 2001", @"description": @"In qui eleifend qualisque eloquentiam, pro audiam utamur at, est ei dicunt dignissim. Mea ea mentitum similique. Ex duo malis summo constituam. Has et etiam scribentur. Ad pri lorem maluisset. Nam magna atqui nobis te."},
                      @{@"title": @"Ad duo clita", @"subtitle": @"Tajo Espiritu, 2012", @"description": @"Nisl case perpetua ut qui, in cum saepe democritum signiferumque. Te vix affert nemore voluptua, usu inani epicuri instructior ut, probo labores noluisse ea sit. His et ignota pertinacia, duo quidam voluptaria consetetur an, id has ignota dissentiet eloquentiam. Qui atqui saperet recteque ad, ei pro falli gloriatur. Ius ei mentitum invenire expetendis."}
                      ];
    }
    return self;
}

- (NSUInteger)indexForBeacon:(bBeacon*)beacon {
    NSUInteger index = 0;
    for(bBeacon* usedBeacon in self.usedBeacons) {
        if([[self class] bBeaconEquals:beacon with:usedBeacon]) {
            return index;
        }
        index++;
    }
    
    if(self.usedBeacons.count == self.data.count) {
#warning it should be -1
        return 0;
    }
    
    [self.usedBeacons addObject:beacon];
    return (self.usedBeacons.count-1);
}

- (UIImage*)imageForBeacon:(bBeacon*)beacon {
    NSUInteger index = [self indexForBeacon:beacon];
    return [UIImage imageNamed:[NSString stringWithFormat:@"exhibit%d.png", index]];
}

- (NSString*)titleForBeacon:(bBeacon*)beacon {
    NSUInteger index = [self indexForBeacon:beacon];
    
    NSDictionary *data = [self.data objectAtIndex:index];
    return [data objectForKey:@"title"];
}

- (NSString*)subtitleForBeacon:(bBeacon*)beacon {
    NSUInteger index = [self indexForBeacon:beacon];
    NSDictionary *data = [self.data objectAtIndex:index];
    return [data objectForKey:@"subtitle"];
}

- (NSString*)descriptionForBeacon:(bBeacon*)beacon {
    NSUInteger index = [self indexForBeacon:beacon];
    NSDictionary *data = [self.data objectAtIndex:index];
    return [data objectForKey:@"description"];
}

@end
