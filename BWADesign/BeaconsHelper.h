//
//  BeaconsHelper.h
//  BWA
//
//  Created by Artur Jaworski on 25.05.2014.
//  Copyright (c) 2014 brckt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "bBeacon.h"

@interface BeaconsHelper : NSObject

+ (instancetype)sharedInstance;
+ (BOOL)bBeaconEquals:(bBeacon*)beacon with:(bBeacon*)otherBeacon;
- (UIImage*)imageForBeacon:(bBeacon*)beacon;
- (NSString*)titleForBeacon:(bBeacon*)beacon;
- (NSString*)subtitleForBeacon:(bBeacon*)beacon;
- (NSString*)descriptionForBeacon:(bBeacon*)beacon;

@end
