//
//  ExplorationViewController.m
//  BWADesign
//
//  Created by Artur Jaworski on 22.05.2014.
//  Copyright (c) 2014 brckt. All rights reserved.
//

#import "ExplorationViewController.h"
#import "ExhibitViewController.h"
#import "bBeaconRegion.h"
#import "bBeacon.h"
#import "BeaconsHelper.h"

@interface ExplorationViewController ()

@property (weak,nonatomic) ExhibitViewController * exhibitViewController;

@property (nonatomic, strong) bBeaconManager *bBeaconManager;
@property (nonatomic, strong) bBeacon *nearestBeacon;

@end

@implementation ExplorationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.exhibitViewControllerWrapper.hidden = YES;
    self.instruction.hidden = NO;
    
    self.bBeaconManager = [[bBeaconManager alloc] init];
    self.bBeaconManager.delegate = self;
    self.bBeaconManager.showUnknownStateBeacons = NO;
    
    bBeaconRegion *region = [[bBeaconRegion alloc] initRegionWithIdentifier:@"BWA"];
    [self.bBeaconManager startRangingBeaconsInRegion:region];
    [self.bBeaconManager startMonitoringForRegion:region];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ExhibitViewControllerSegue"]) {
        self.exhibitViewController = segue.destinationViewController;
    }
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

#pragma mark - bBeaconManagerDelegate

- (void)beaconManager:(bBeaconManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(bBeaconRegion *)region {
    bBeacon *nearestBeacon = nil;
    BOOL foundLastNearestBeacon = NO;
    for (bBeacon *beacon in beacons) {
        if([BeaconsHelper bBeaconEquals:self.nearestBeacon with:beacon]) {
            self.nearestBeacon = beacon;
            foundLastNearestBeacon = YES;
        }
        
        if(beacon.iBeacon.accuracy < 0) {
            continue;
        }
        
        if(beacon.iBeacon.proximity != CLProximityImmediate && beacon.iBeacon.proximity != CLProximityNear) {
            continue;
        }
        
        if(!nearestBeacon || beacon.iBeacon.accuracy < nearestBeacon.iBeacon.accuracy) {
            nearestBeacon = beacon;
        }
    }

    if(!foundLastNearestBeacon || !nearestBeacon) {
        self.nearestBeacon = nil;
    }
    
    if(!nearestBeacon) {
        [self showInfoForNoBeacon];
        return;
    }
    
    if([BeaconsHelper bBeaconEquals:self.nearestBeacon with:nearestBeacon]) {
        return;
    }
    
    if(self.nearestBeacon.iBeacon.accuracy*1.0 > nearestBeacon.iBeacon.accuracy) {
        return;
    }
    
    self.nearestBeacon = nearestBeacon;
    [self showInfoForBeacon:self.nearestBeacon];
}

- (void)beaconManager:(bBeaconManager *)manager didExitRegion:(bBeaconRegion *)region {
    if([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive) {
        return;
    }
    
    [self showNotification:@"Dziękujęmy za odwiedzenie BWA."];
}

- (void)beaconManager:(bBeaconManager *)manager didEnterRegion:(bBeaconRegion *)region {
    if([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive) {
        return;
    }
    
    [self showNotification:@"Otwórz aplikację, aby rozpocząć eksplorację galerii."];
}

- (void)showNotification:(NSString*)text {
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    notification.alertBody = text;
    notification.soundName = UILocalNotificationDefaultSoundName;
    
    [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
}

- (void)showInfoForNoBeacon {
    self.exhibitViewControllerWrapper.hidden = YES;
    self.instruction.hidden = NO;
}

- (void)showInfoForBeacon:(bBeacon*)beacon {
    self.exhibitViewControllerWrapper.hidden = NO;
    self.instruction.hidden = YES;
    self.exhibitViewController.beacon = beacon;
}

@end
